"""
GPS Qualidade:
    0 = invalid
    1 = GPS fix (SPS)
    2 = DGPS fix
    3 = PPS fix
	4 = Real Time Kinematic
	5 = Float RTK
    6 = estimated (dead reckoning) (2.3 feature)
	7 = Manual input mode
	8 = Simulation mode
"""

import serial
import pynmea2
#https://github.com/Knio/pynmea2
from LoggErro import Log
import sys

erro = Log()

class Gps(object):
    latitude=0
    longitude=0
    def __init__(self):
        pass

    def get_latlog(self):
        try:
            ser = serial.Serial('/dev/ttyACM0', 4800, timeout=1)
            while True:
                line = ser.readline()
    
                if b"GPGGA" in line:
                    msg = pynmea2.parse(line.decode("utf-8"))
                    return (msg.latitude,msg.longitude,msg.gps_qual,msg.num_sats)
        except OSError as err:
            erro.grava("GPS.OS error: {0}".format(err))
            return (-1,-1,-1,-1)
            #print("OS error: {0}".format(err))
        except:
            #print("Erro: ", sys.exc_info()[0])
            erro.grava("GPS.Erro: {0}".format(sys.exc_info()[0]))
            return (-1,-1,-1,-1)
    
