from openalpr import Alpr
import cv2
from def_salva import Salva
from def_api import Api
from def_gps import Gps
from argparse import ArgumentParser
import sys


parser = ArgumentParser(description='T-Recis programa OCR da WMB')

parser.add_argument("-c",
                    dest="camera_name", action="store", default="Cam_001",required = True,
                    help="[string] Nome da câmera" )
parser.add_argument("-d",type=int,
                    dest="device_id", action="store", default="0",required = False,
                    help="[int] Device Id" )
parser.add_argument("-f", type=int,
                    dest="frame_skip", action="store", default="5",required = False,
                    help="[int] Qtd. fps a processar.Ex:5 processa até 5fps depois skip - DISABLED" )
parser.add_argument("-n",type=int,
                    dest="top_n", action="store", default="1",required = False,
                    help="[int] Qtd. de placas retornadas por imagem" )
parser.add_argument("-t",type=int,
                    dest="threads", action="store", default="1",required = False,
                    help="[int] Nº de threads para processar" )
parser.add_argument("--no-gpu",
                    dest="gpu", action="store_false", default=True,required = False,
                    help="[flag] Se chamada não roda por GPU e sim por CPU" )
options = parser.parse_args()


FRAME_SKIP = options.frame_skip

def main():
    print(chr(27) + "[2J")
    if(options.gpu):
        print('Executando com\033[92m NVIDIA CUDA\033[0m desabilitando uso de CPU' )

    placas = []
    api = Api(options.camera_name)
    gps = Gps()

    #Linux
    alpr = Alpr("br", "/etc/openalpr/openalpr.conf", "/usr/share/openalpr/runtime_data","",options.gpu,0,10)
    #Windows
    #alpr = Alpr("br", r"D:\openalpr64-sdk-2.7.102\openalpr.conf", r"D:\openalpr64-sdk-2.7.102\runtime_data")
    if not alpr.is_loaded():
        print('Erro ao carregar OpenALPR')
        sys.exit(1)
    
    alpr.use_gpu = options.gpu
    alpr.gpu_id = 0
    alpr.set_top_n(options.top_n)
    alpr.motion_detection = 1
    alpr.set_default_region('br')
    alpr.analysis_threads = options.threads

    cap = cv2.VideoCapture(options.device_id)
    if not cap.isOpened():
        alpr.unload()
        sys.exit('Falha ao carregar o video!')

    frame_number =0
    while True:
        ret_val, frame = cap.read()
        if not ret_val:
            print('VidepCapture.read() falhou. Saindo....')
            break

        frame_number += 1
        # if frame_number % FRAME_SKIP == 0:
        #     continue

        results = alpr.recognize_ndarray(frame)
        for i, plate in enumerate(results['results']):
            melhor_placa = plate['candidates'][0]
            placa = melhor_placa['plate'].upper()

            coord_ini_x = 1919 if plate['coordinates'][0]['x'] > 1920 else plate['coordinates'][0]['x']
            coord_ini_y = plate['coordinates'][0]['y']
            coord_fim_x = 1919 if plate['coordinates'][2]['x'] > 1920 else plate['coordinates'][2]['x']
            coord_fim_y = plate['coordinates'][2]['y']
            if placa not in placas:
                if len(placa) == 7:
                    acuracia = melhor_placa['confidence']
                    placas.append(placa)
                    save = Salva(placa)
                    # cv2.imshow("Teste img 01", frame)
                    # cv2.waitKey(0)
                    nome_file = save.SalvaFullImage(frame)
                    crop_placa = frame[coord_ini_y:coord_fim_y, coord_ini_x:coord_fim_x]
                    save.SalvaCropImage(crop_placa)
                    gps_lat, gps_log, gps_qual, _ = gps.get_latlog()
                    api.send(placa,
                            acuracia,
                            '{},{}'.format(coord_ini_x, coord_ini_y), 
                            '{},{}'.format(coord_fim_x, coord_fim_y),
                            gps_lat, 
                            gps_log,
                            gps_qual, 
                            nome_file)
                    print('{:7s} ({:.2f}%) - {:.2f}ms'.format(placa, acuracia, plate['processing_time_ms']))
                    

        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()
    cap.release()
    alpr.unload()

if __name__ == "__main__":
    main()