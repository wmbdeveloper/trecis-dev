import logging
from datetime import datetime


timestr = datetime.now().strftime("%Y-%m-%d")
NAME_FILE = "TRECIS_{}.log".format(timestr)
logging.basicConfig(filename='/tmp/{}'.format(NAME_FILE), level=logging.DEBUG, 
                    format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger=logging.getLogger(__name__)



class Log(object):
    def grava(self, erro):
        logger.error(erro)
