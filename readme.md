# Instalar o OpenAlpr
gerar uma licença trial 14 dias
https://license.openalpr.com/auth_key_register/

Depois instalar o app
 ```bash <(curl https://deb.openalpr.com/install) ```

* instalar (agente, sdk e cuda (se tiver nvidia))*




Para testar se está tudo ok:

    sudo wget http://plates.openalpr.com/ea7the.jpg
    sudo alpr -c us ea7the.jpg** 
 
 
##  Configurando Python e suas dependencias
    python3 -m pip install --upgrade pip
    pip install --upgrade pip
    pip3 install openalpr
    pip3 install numpy
    pip3 install opencv-python
    pip3 install pynmea2
    pip3 install pyserial
    pip3 install requests
    
    sudo apt-get install gpsd gpsd-clients python-gps